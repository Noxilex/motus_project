const assert = require('assert');
const { Given, When, Then } = require('cucumber');
const Game = require('../../game.js');

Given('the game has started', function () {
    this.game = {
        motus: undefined
    }
    Game.startGame(this.game);
});

Given('the MOTUS is {string}', function (motus) {
    this.game.motus = motus;
});

When('user enter {string}', function () {
    Game.userInput(this.game);
});

Then('the user win the game', function () {
    assert(this.game.win == true);
});

Then('cell {int} is {string}', function (int, string) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Given('user try {int} times', function (int) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('user enter bad word', function () {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the game continue', function () {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('the game stop', function () {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

When('user enter {int} letters word', function (int) {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Then('a warning appear', function () {
    // Write code here that turns the phrase above into concrete actions
    return 'pending';
});

Given('the game has not started', function () {
    this.game = {
        motus: undefined
    }
});

When('user will start the game', function () {
    Game.startGame(this.game);
});

Then('the Motus with {int} letters has been chosen', function (motusLength) {
    assert(this.game.motus && this.game.motus.length == motusLength);
});