Feature: validate word
	user have to enter 8 letters word

	Scenario: 7 letters
		Given the game has started
		When user enter 7 letters word
		Then a warning appear

	Scenario: 8 letters
		Given the game has started
		When user enter 8 letters word
		Then the game continue

	Scenario: 9 letters
		Given the game has started
		When user enter 9 letters word
		Then a warning appear