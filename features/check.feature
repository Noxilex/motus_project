Feature: word check
	system compare MOTUS And the given word

	Scenario: WIN
		Given the game has started 
		  And the MOTUS is "ALIMENTS"
		When user enter "ALIMENTS"
		Then the user win the game 

	Scenario: FAIL
		Given the game has started 
		  And the MOTUS is "ALIMENTS"
		When user enter "ABRICOTS"
		Then cell 1 is "green"
		 And cell 6 is "green"
		 And cell 7 is "green"
		 And cell 4 is "yellow"